#  4*4 矩阵键盘


![](./arduinoC/_images/featured.png)

---------------------------------------------------------

## 目录

* [相关链接](#相关链接)
* [描述](#描述)
* [积木列表](#积木列表)
* [示例程序](#示例程序)
* [许可证](#许可证)
* [支持列表](#支持列表)
* [更新记录](#更新记录)

## 相关链接
* 本项目加载链接: 

  ```Gitee 地址（适合国内用户）：https://gitee.com/hockel/keypad```

  ```github 地址：https://github.com/huncker/keypad```

* 用户库教程链接: ```https://mindplus.dfrobot.com.cn/extensions-user```

* Mind+ 软件下载地址：http://mindplus.cc

* 购买此产品: [某宝](https://detail.tmall.com/item.htm?id=38046624610&ali_refid=a3_430582_1006:1107554683:N:Y6Ct8BuhS0EMzyQvhLnsMw==:6474400762360146d1a215594b0600e5&ali_trackid=1_6474400762360146d1a215594b0600e5&spm=a230r.1.14.1)或dfrobot官网.

## 描述
针对arduino写的矩阵键盘，支持键盘输入，密码验证等功能

## 电路连接图

![](./arduinoC/_images/temp.png)

## 积木列表

![](./arduinoC/_images/blocks.png)



## 示例程序

##  示例一：串口输出按键值

![](./arduinoC/_images/example.png)

实验结果：

![](./arduinoC/_images/print.png)

## 示例二：验证密码

![](./arduinoC/_images/example1.png)

实验结果：

![](./arduinoC/_images/print1.png)

详细开发文档请参考此文章：[【mind+用户库编写】4*4矩阵键盘使用教程](https://mc.dfrobot.com.cn/thread-306125-1-1.html)

## 许可证

MIT

## 支持列表

主板型号                | 实时模式    | ArduinoC   | MicroPython    | 备注
------------------ | :----------: | :----------: | :---------: | -----
Arduino        |             |       √       |             | 


## 更新日志
* V0.0.1  基础功能完成
* v0.0.2 调式了某些bug