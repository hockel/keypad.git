

//% color="#436EEE" iconWidth=50 iconHeight=40
namespace keypad {
    //% block="矩阵键盘行初始化:行1:[H1]行2:[H2]行3:[H3]行4:[H4]" blockType="command"
    //% H1.shadow="dropdown" H1.options="PIN_DigitalWrite" H1.defl="PIN_DigitalWrite.default_keypadInit_H1"
    //% H2.shadow="dropdown" H2.options="PIN_DigitalWrite" 
    //% H3.shadow="dropdown" H3.options="PIN_DigitalWrite" 
    //% H4.shadow="dropdown" H4.options="PIN_DigitalWrite" 
    export function keypadRowInit(parameter: any, block: any) {
       let row1 = parameter.H1.code;
       let row2 = parameter.H2.code;
       let row3 = parameter.H3.code;
       let row4 = parameter.H4.code;

    Generator.addInclude('keypad_include', '#include <Keypad.h>');
    Generator.addObject(`keypad_row`, `const byte`, `KEYPAD_4_4_ROWS = 4;`);
    Generator.addObject(`keypad_cols`, `const byte`, `KEYPAD_4_4_COLS = 4;`);
    Generator.addObject(`keypad_hexakey`, `char`,       `KEYPAD_4_4_hexaKeys[KEYPAD_4_4_ROWS][KEYPAD_4_4_COLS] = {\n\t {'1','2','3','A'},\n\t {'4','5','6','B'},\n\t {'7','8','9','C'},\n\t {'*','0','#','D'}\n\t};`);
    Generator.addObject(`keypad_rowpin`, `byte`, `KEYPAD_4_4_rowPins[KEYPAD_4_4_ROWS] = {${row1}, ${row2},${row3},${row4}};`);
    }
    
    //% block="矩阵键盘列初始化:列1:[S1]列2:[S2]列3:[S3]列4:[S4]" blockType="command"
    //% S1.shadow="dropdown" S1.options="PIN_DigitalWrite" S1.defl="PIN_DigitalWrite.default_lie1"
    //% S2.shadow="dropdown" S2.options="PIN_DigitalWrite" S2.defl="PIN_DigitalWrite.default_lie2"
    //% S3.shadow="dropdown" S3.options="PIN_DigitalWrite" S3.defl="PIN_DigitalWrite.default_lie3"
    //% S4.shadow="dropdown" S4.options="PIN_DigitalWrite" S4.defl="PIN_DigitalWrite.default_lie4"
    export function keypadcolInit(parameter: any, block: any) {
        let col1 = parameter.S1.code;
        let col2 = parameter.S2.code;
        let col3 = parameter.S3.code;
        let col4 = parameter.S4.code;
 

     
     Generator.addObject(`keypad_colpin`, `byte`, `KEYPAD_4_4_colPins[KEYPAD_4_4_COLS] = {${col1}, ${col2},${col3},${col4}};`);
     Generator.addObject(`keypad_obj`, `Keypad `, `KEYPAD_4_4 = Keypad( makeKeymap(KEYPAD_4_4_hexaKeys), KEYPAD_4_4_rowPins, KEYPAD_4_4_colPins, KEYPAD_4_4_ROWS, KEYPAD_4_4_COLS);`);
     Generator.addSetup("keypad_begin", `Serial.begin(9600);`,true);
     }


    //% block="获取矩阵键盘按键数值 " blockType="reporter"
export function keypadPress(parameter: any, block: any) {
    Generator.addCode(["KEYPAD_4_4.getKey()", Generator.ORDER_UNARY_POSTFIX]);
}

}